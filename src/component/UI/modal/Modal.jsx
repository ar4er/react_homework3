import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Button from "../button/Button";
import modalSettings from "./modalSettings";
import "./Modal.scss";

function Modal({ modalId, closeModal, submitFunction, data, visible }) {
  const [settings, setSettings] = useState({});
  useEffect(() => {
    const modal = modalSettings.find((item) => item.modalId === modalId);
    setSettings(modal.settings);
  }, [modalId, visible]);

  const { header, closeButton, text, actions } = settings;
  return (
    <div className={visible ? "modal active" : "modal"} onClick={closeModal}>
      <div
        className={visible ? "modal-content active" : "modal-content"}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="modal-header">
          {header}
          {closeButton && (
            <Button text="X" background="magenta" onClick={closeModal} />
          )}
        </div>
        <div className="modal-body">{text && text(data)}</div>
        <div className="modal-footer">
          {actions &&
            actions.map((item, index) => (
              <Button
                text={item.text}
                onClick={
                  item.type === "submit"
                    ? () => {
                        submitFunction();
                        closeModal();
                      }
                    : closeModal
                }
                key={Date.now() + index}
              />
            ))}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  visible: PropTypes.bool,
  modalId: PropTypes.string,
  submit: PropTypes.func,
  closeModal: PropTypes.func,
};

export default Modal;
