import PropTypes from "prop-types";
import Product from "../product/Product";
import styles from "./ProductList.module.scss";

function ProductList({
  products,
  updateBasket,
  updateFavorite,
  openModal,
  page,
}) {
  return (
    <div className={styles.ProductList}>
      {products.map((product) => (
        <Product
          product={product}
          updateFavorite={updateFavorite && updateFavorite}
          updateBasket={updateBasket && updateBasket}
          openModal={openModal && openModal}
          page={page}
          key={product.article}
        />
      ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.array,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};

export default ProductList;
