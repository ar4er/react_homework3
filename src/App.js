import { useState, useEffect } from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import styles from "./styles/index.module.scss";
import Modal from "./component/UI/modal/Modal";
import { Basket, Favorite, Home } from "./pages";
import favoriteIcon from "./image/favourites-star.png";
import basketIcon from "./image/basket.png";
import homeIcon from "./image/home.png";

const App = () => {
  const [products, setProducts] = useState([]);
  const [productsInFavorite, setProductsInFavorite] = useState([]);
  const [productsInBasket, setProductsInBasket] = useState([]);
  const [modal, setModal] = useState({
    visible: false,
    modalId: "default",
    submitFunction: null,
    data: null,
  });

  useEffect(() => {
    fetch("productCollection.json")
      .then((response) => response.json())
      .then((products) => setProducts(products));

    localStorage.getItem("productsInFavorite")
      ? setProductsInFavorite(
          JSON.parse(localStorage.getItem("productsInFavorite"))
        )
      : localStorage.setItem("productsInFavorite", JSON.stringify([]));

    localStorage.getItem("productsInBasket")
      ? setProductsInBasket(
          JSON.parse(localStorage.getItem("productsInBasket"))
        )
      : localStorage.setItem("productsInBasket", JSON.stringify([]));
  }, []);

  const updateFavorite = () => {
    setProductsInFavorite(
      JSON.parse(localStorage.getItem("productsInFavorite"))
    );
  };

  const updateBasket = () => {
    setProductsInBasket(JSON.parse(localStorage.getItem("productsInBasket")));
  };

  const openModal = (modalId, submitFunction, data) => {
    setModal({
      visible: true,
      modalId,
      submitFunction,
      data,
    });
  };

  const closeModal = () => {
    setModal({
      visible: false,
      modalId: "default",
      submitFunction: null,
      data: null,
    });
  };

  return (
    <div className={styles.App}>
      <nav className={styles.productCounts}>
        <NavLink to="/" className={styles.navigateIcon}>
          <img src={homeIcon} alt="Home" />
        </NavLink>
        <NavLink to="/favorites" className={styles.navigateIcon}>
          <img src={favoriteIcon} alt="Favorite" />
          <span className={styles.productCount}>
            {productsInFavorite.length}
          </span>
        </NavLink>
        <NavLink to="/basket" className={styles.navigateIcon}>
          <img src={basketIcon} alt="Basket" />
          <span className={styles.productCount}>{productsInBasket.length}</span>
        </NavLink>
      </nav>
      <Routes>
        <Route
          path="/"
          element={
            <Home
              products={products}
              updateFavorite={updateFavorite}
              updateBasket={updateBasket}
              openModal={openModal}
              page="home"
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favorite
              products={productsInFavorite}
              updateFavorite={updateFavorite}
              updateBasket={updateBasket}
              openModal={openModal}
              page="favorite"
            />
          }
        />
        <Route
          path="/basket"
          element={
            <Basket
              products={productsInBasket}
              updateFavorite={updateFavorite}
              updateBasket={updateBasket}
              openModal={openModal}
              page="basket"
            />
          }
        />
      </Routes>
      <Modal
        visible={modal.visible}
        modalId={modal.modalId}
        submitFunction={modal.submitFunction}
        data={modal.data}
        closeModal={closeModal}
      />
    </div>
  );
};

export default App;
