import React from "react";
import ProductList from "../component/product-list/ProductList";
export function Home({ products, updateFavorite, updateBasket, openModal, page }) {
  return (
    <div>
      <ProductList
        products={products}
        updateFavorite={updateFavorite}
        updateBasket={updateBasket}
        openModal={openModal}
        page={page}
      />
    </div>
  );
}
