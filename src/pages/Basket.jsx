import ProductList from "../component/product-list/ProductList";

export function Basket({
  updateBasket,
  updateFavorite,
  openModal,
  products,
  page,
}) {
  return products.length ? (
    <>
      <ProductList
        products={products}
        updateBasket={updateBasket}
        updateFavorite={updateFavorite}
        openModal={openModal}
        page={page}
      />
    </>
  ) : (
    "Корзина порожня ):"
  );
}
